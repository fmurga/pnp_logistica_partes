<?php

namespace Database\Seeders;
use App\Models\Product;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $product = new Product();
        $product->name='Laptop';
        $product->description='Es una laptop ASUS';
        $product->save();

        $product2 = new Product();
        $product2->name='Celular';
        $product2->description='Es un celular Samsung';
        $product2->save();

        $product3 = new Product();
        $product3->name='Monitor';
        $product3->description='Es un monitor HP';
        $product3->save();
    }
}
