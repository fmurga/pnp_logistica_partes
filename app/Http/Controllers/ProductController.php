<?php

namespace App\Http\Controllers;
use App\Http\Controllers\QrCode;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    //

    public function index(){

        $productos = Product::orderBy('id','DESC')->get();

        return view('productos.index', compact('productos'));
    }
    public function indexQr(){


    

        return view('productos.productosQr');
    }


    public function show(){


        return 'Bienvenido a la pagina principal';
    }
}
